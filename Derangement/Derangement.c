#include<stdio.h>

int main()				//main function

{
	int i, j, k, l, m, n, o, count=0, no, x;
	printf("Enter no for finding derangements : (<=7) ");
	scanf("%d", &no);
	for(i=1; i<=no; i++)		//loop for finding first derangement
	{
		if(i!=1)
		{	
			if(no == 1)		
			{
				printf(" %d", i);
				printf("\n");
				count++;
			}
			for(j=1; j<=no; j++)	//loop for finding derangement of 2 no's
			{
				if(j!=i && j!=2)
				{
					if(no == 2)
					{
						printf(" %d %d", i, j);
						printf("\n");
						count++;
					}
					for(k=1; k<=no; k++)		//loop for finding derangement of 3 no's
					{
						if(k!=j && k!=i && k!=3)
						{
								if(no == 3)
								{
									printf(" %d %d %d", i, j, k);
									printf("\n");
									count++;
								}
							for(l=1; l<=no; l++)		//loop for finding derangement of 4 no's
							{
								if(l!=k && l!=j && l!=i && l!=4)
								{
									if(no == 4)
									{
										printf(" %d %d %d %d", i, j, k, l);
										printf("\n");
										count++;
									}
									for(m=1; m<=no; m++)		//loop for finding derangement of 5 no's
									{
										if(m!=l && m!=k && m!=j && m!=i && m!=5)
										{
											if(no == 5)
											{
												printf(" %d %d %d %d %d", i, j, k, l, m);
												printf("\n");
												count++;
											}
											for(n=1; n<=no; n++)		//loop for finding derangement of 6 no's
											{
												if(n!=m && n!=l && n!=k && n!=j && n!=i && n!=6)
												{
													if(no == 6)
														{
															printf(" %d %d %d %d %d %d", i, j, k, l, m, n);
															printf("\n");
															count++;
														}
													for(o=1; o<=no; o++)		//loop for finding derangement of 7 no's
													{
														if(o!=n && o!=m && o!=l && o!=k && o!=j && o!=i && o!=7)
														{
															if(no == 7)
															{
																printf("%d%d%d%d%d%d%d , ", i, j, k, l, m, n, o);
																count++;
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}		
	}
	if(no>7)		//if no >7 then the main function is called again and asks the user to enter no again
	{	
		printf("No is out of domain\n\n");
		main();
	}
	else			//if no <=7 then it prints no of derangements from count data member
	{
		printf("\nNo of derangements possible are : %d\n\n", count);		
	}
	return 0;
}