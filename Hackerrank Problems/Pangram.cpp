#include<iostream>

using namespace std;

class Pangram
{
    private:
    string s1;
    int value[26];
    public:
    Pangram();
    void setValue(string);
    bool pangramCalculator();
};

Pangram :: Pangram()
{
    for(int i=0; i<26; i++)
    {
        value[i] = 0;
    }
}

void Pangram :: setValue(string input)
{
    s1 = input;
}

bool Pangram :: pangramCalculator()
{
    int temp = 0;
    for(int i=int('a'), l=int('A'), k=0; i<=int('z'); i++, k++, l++)
    {
        for(int j=0; j<s1.length(); j++)
        {
            if(s1[j] == char(i) || s1[j] == char(l))
            {
                value[k]++;
            }
        }
    }
    for(int l=0; l<26; l++)
    {
        if(value[l] == 0)
        {
            temp = -1;
            break;
        }
    }
    if(temp == -1)
        return false;
    else
        return true;
}

int main()
{
    string stringInput;
    getline(cin, stringInput);
    Pangram p;
    p.setValue(stringInput);
    if(p.pangramCalculator())
        cout<<"pangram";
    else
        cout<<"not pangram";
}


