#include<iostream>
#include<cstdlib>

using namespace std;

int highIndex(float* array, int size){
	float max = array[0];
	int index = 0;
	for(int z=0; z<size; z++){
		if(array[z] > max){
			max = array[z];
			index = z;
		}
	}
	return index;
}

int lowIndex(float* array, int size){
	float min = array[0];
	int index = 0;
	for(int z=0; z<size; z++){
		if(array[z] < min){
			min = array[z];
			index = z;
		}
	}
	return index;
}



int main(){
	int T, N, temp, counter=0;
	float* marks;
	bool flag = false;
	cin>>T;
	for(int i=0; i<T; i++){
		cin>>N;
		marks = new float[N];
		float* aptitudeMarks[5];
		for(int f=0; f<5; f++)
			aptitudeMarks[f] = new float[N];
		for(int j=0; j<N; j++){
			cin>>marks[j];
		}
		for(int k=0; k<5; k++){
			for(temp=0; temp<N; temp++){
				cin>>aptitudeMarks[k][temp];
			}
		}
		for(int y=0; y<5; y++){
			counter++;
			if((highIndex(aptitudeMarks[y], N) == highIndex(marks, N)) && (lowIndex(aptitudeMarks[y], N) == lowIndex(marks, N))){
				cout<<counter;
				flag = true;
				break;
			}
		}		
		if(flag == false)
			cout<<counter;
		counter = 0;
		flag = false;
		cout<<"\n";
	}
}