#include<iostream>
#include<cstdlib>
#include<cmath>

using namespace std;

int main()
{
	int *no;
	int T, N, result, i, j;
	cin>>T;
	for(i=0; i<T; i++)
	{
		cin>>N;
		no = new int[N];
		for(j=0; j<N; j++)
		{
			cin>>no[j];
		}
		if(N % 2 == 0)
		{
			result = 0;
		}
		else
		{
			for(j=0, result = 0; j<N; j++)
			{
				if(j%2 == 0)
					result ^= no[j];
			}
		}
		cout<<result<<"\n";
	}
}