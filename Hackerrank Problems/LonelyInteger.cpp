#include<iostream>

using namespace std;

class LonelyInteger
{
    private:
    int size;
    int *value;
    public:
    LonelyInteger(int);
    void setValue(int, int);
    int lonelyCounter();
};

LonelyInteger :: LonelyInteger(int sizeX)
{
    size = sizeX;
    value = new int[size];
}

void LonelyInteger :: setValue(int position, int no)
{
    value[position] = no;
}

int LonelyInteger :: lonelyCounter()
{
    int count, i;
    for(i=0; i<size; i++)
    {
        count = 1;
        for(int j=0; j<size; j++)
        {
            if(value[i] == value[j] && j!=i)
            {
                count++;
            }
        }
        if(count == 1)
        {
            break;
        }
    }
    return value[i];
}

int main()
{
    int sizeInput, no;
    cin>>sizeInput;
    LonelyInteger l(sizeInput);
    for(int k=0; k<sizeInput; k++)
    {
        cin>>no;
        l.setValue(k, no);
    }
    cout<<l.lonelyCounter()<<"\n";
    return 0;
}
