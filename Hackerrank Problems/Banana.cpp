#include<iostream>
#include<cstdlib>

using namespace std;

string translator(string inputX, string* encryptX, string* decryptX, int size){
	for(int z=0; z<size; z++){
		if(inputX == encryptX[z])
			return decryptX[z];	
	}
	return NULL;
}

int main(){
	string* encrypt;
	string* equalString;
	string* decrypt;
	string* input;
	string* output;
	int T, K, N;
	cin>>N;
	encrypt = new string[N];
	decrypt = new string[N];
	equalString = new string[N];
	for(int i=0; i<N; i++){
		cin>>encrypt[i];
		cin>>equalString[i];
		cin>>decrypt[i];
	}
	cin>>T;
	for(int j=0; j<T; j++){
		cin>>K;
		input = new string[K];
		output = new string[K];
		for(int k=0; k<K; k++){
			cin>>input[k];
			output[k] = translator(input[k], encrypt, decrypt, N);
		}
		for(int y=0; y<K; y++){
			cout<<output[y]<<" ";
		}
		cout<<"\n";
	}
	return 0;
}