#include<iostream>

using namespace std;

class Tree
{
    private:
    int height;
    int cycles;
    public:
    Tree();
    int growth(int);
};

Tree :: Tree()
{
    height = 1;
}

int Tree :: growth(int cyclesInput)
{
    for(int i=0; i<cyclesInput; i++)
    {
        if(i%2 == 0)
            height = 2*height;
        else
            height++;
    }
    return height;
}

int main()
{
    int T, N;
    cin>>T;
    for(int i=0; i<T; i++)
    {
        cin>>N;
        Tree t;
        cout<<t.growth(N)<<"\n";
    }
}
