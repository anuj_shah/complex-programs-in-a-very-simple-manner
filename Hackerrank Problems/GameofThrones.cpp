#include<iostream>

using namespace std;

class GameofThrones
{
	private:
	string input;
	int counter[26];
	public:
	void setInput(string);
	void Counter();
	bool checker();
};

void GameofThrones :: setInput(string inputX)
{
	input = inputX;
}

void GameofThrones :: Counter()
{
	int i, j, k;
	for(k=0; k<26; k++)
    {
        counter[k] = 0;
    }
	for(i=int('a'), k=0; i<=int('z'); i++, k++)
	{
		for(j=0; j<=input.length(); j++)
		{
			if(input[j] == char(i))
			{
				counter[k]++;
			}
		}
	}
}

bool GameofThrones :: checker()
{
	int k, counterO;
	for(k=0, counterO=0; k<26; k++)
	{
		if(counter[k]%2 !=0)
		{
			counterO++;
		}
		if(counterO>1)
		{
			return false;
		}
	}
	return true;
}

int main()
{
	string inputY;
	GameofThrones game;
	getline(cin, inputY);
	game.setInput(inputY);
	game.Counter();
	if(game.checker())
	{
		cout<<"YES\n";
	}
	else
	{
		cout<<"NO\n";
	}
}
