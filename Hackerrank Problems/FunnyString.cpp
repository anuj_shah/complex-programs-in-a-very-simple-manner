#include<iostream>
#include<cmath>

using namespace std;

class FunnyStrings
{
    private:
    string s1;
    string rs1;
    public:
    void setValue(string);
    void reverseString();
    bool funnyString();
};

void FunnyStrings :: setValue(string input)
{
    s1 = input;
    rs1 = s1;
}

void FunnyStrings :: reverseString()
{
    for(int i=0, j=s1.length(); i<s1.length(); i++, j--)
    {
        rs1[j-1] = s1[i];
    }
}

bool FunnyStrings :: funnyString()
{
    int temp=0;
    for(int i=0; i<s1.length()-1; i++)
    {
        if(abs(int(s1[i+1]) - int(s1[i])) != abs(int(rs1[i+1]) - int(rs1[i])))
        {
            temp = -1;
            break;
        }
    }
    if(temp == -1)
        return false;
    else
        return true;
}

int main()
{
    int T;
    string stringInput;
    cin>>T;
    for(int i=0; i<T; i++)
    {
        cin>>stringInput;
        FunnyStrings f;
        f.setValue(stringInput);
        f.reverseString();
        if(f.funnyString())
            cout<<"Funny";
        else
            cout<<"Not Funny";
        cout<<"\n";
    }
    return 0;
}
