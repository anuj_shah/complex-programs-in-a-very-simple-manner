#include<iostream>
#include<cmath>

using namespace std;

class FlippingBits
{
    private:
    unsigned long int no;
    int bit[32];
    unsigned long int count;
    public:
    FlippingBits();
    void setValue(unsigned long int);
    unsigned long int flippingCalculator();
};

FlippingBits :: FlippingBits()
{
    for(int i=0; i<32; i++)
        bit[i] = 0;
    count = 0;
}

void FlippingBits :: setValue(unsigned long int input)
{
    no = input;
}

unsigned long int FlippingBits :: flippingCalculator()
{
    unsigned long int tempCount = no;
    for(int k=0; ; k++)
    {
        bit[k] = tempCount%2;
        tempCount = tempCount/2;
        if(tempCount<1)
            break;
    }
    for(int i=0; i<32; i++)
    {
        if(bit[i] == 0)
            bit[i] = 1;
        else
            bit[i] = 0;
    }
    for(int i=0; i<32; i++)
    {
        count += pow(2, i)*bit[i];
    }
    return count;
}

int main()

{
    int T;
    unsigned long int noInput;
    cin>>T;
    for(int i=0; i<T; i++)
    {
        cin>>noInput;
        FlippingBits f;
        f.setValue(noInput);
        cout<<f.flippingCalculator()<<"\n";
    }
}


