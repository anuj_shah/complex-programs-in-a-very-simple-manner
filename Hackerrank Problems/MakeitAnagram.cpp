#include<iostream>
#include<cmath>

using namespace std;

class MakeitAnagram
{
    private:
    string s1;
    string s2;
    int value1[26];
    int value2[26];
    int counter;
    public:
    MakeitAnagram();
    void setValue(string, string);
    int cipherCounter();
};

MakeitAnagram :: MakeitAnagram()
{
    counter = 0;
    for(int i=0; i<26; i++)
    {
        value1[i] = 0;
        value2[i] = 0;
    }
}

void MakeitAnagram :: setValue(string string1, string string2)
{
    s1 = string1;
    s2 = string2;
}

int MakeitAnagram :: cipherCounter()
{
    for(int i=int('a'), k=0; i<=int('z'); i++, k++)
    {
        for(int j=0; j<s1.length(); j++)
        {
            if(s1[j] == char(i))
            {
                value1[k]++;
            }
        }
    }
    for(int i=int('a'), k=0; i<=int('z'); i++, k++)
    {
        for(int j=0; j<s2.length(); j++)
        {
            if(s2[j] == char(i))
            {
                value2[k]++;
            }
        }
    }
    for(int i=0; i<26; i++)
    {
        counter += abs(value1[i] - value2[i]);
    }
    return counter;
}

int main()
{
    string stringInput1, stringInput2;
    MakeitAnagram c;
    getline(cin, stringInput1);
    getline(cin, stringInput2);
    c.setValue(stringInput1, stringInput2);
    cout<<c.cipherCounter()<<"\n";
}
