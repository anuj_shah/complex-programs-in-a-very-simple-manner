/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Anuj Shah
 */
public class NodeCoordinate {   //class to set latitudes and longitudes of the city

    static int counter = 0;
    int NodeNum;
    double xCo, yCo;
    NodeCoordinate next;

    NodeCoordinate(double x, double y) {   //function sets x and y coordinate equal to the latitude and longitude
        xCo = x;
        yCo = y;
        NodeNum = counter + 1;
        this.next = null;
    }
}
