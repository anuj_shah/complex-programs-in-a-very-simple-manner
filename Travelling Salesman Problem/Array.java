/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Anuj Shah
 */
public class Array {

    int len;										//Size of Array
    int arr[];										//arr[] actually stores values

    Array() {
        len = NodeCoordinate.counter - 1;
        arr = new int[len];
    }

    Array(int size) {
        len = size;
        arr = new int[len];
    }

    void fill() {
        for (int i = 0; i < len; i++) {
            arr[i] = i + 2;
        }
    }

    int getlen() //Returns size of Array
    {
        return len;
    }

    int getData(int pos) //Returns value at a specific position
    {
        return arr[pos];
    }

    void fillData(int data, int pos) //Used to set value at specific location
    {
        arr[pos] = data;
    }
}
