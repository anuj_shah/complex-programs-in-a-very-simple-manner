
import java.text.DecimalFormat;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Anuj Shah
 */
public class DistMatrix {

    Scanner input = new Scanner(System.in);
    DecimalFormat df = new DecimalFormat("#.00");	//Used to trim extra digits and round off double values
    NodeCoordinate start, temp, tail;
    double min, minF;
    double[][] distMatrix;

    void initDist() {
        distMatrix = new double[NodeCoordinate.counter][NodeCoordinate.counter];			//Created fixed Matrix(can be later converted to generic)
        convertToMatrix();
    }

    void createNode(double x, double y) {
        /*System.out.println("Enter x Co-ordinate");
         int x = input.nextInt();
         System.out.println("Enter y-Co-ordinate");
         int y = input.nextInt();*/
        temp = new NodeCoordinate(x, y);
        if (NodeCoordinate.counter == 0) {
            start = tail = temp;
        } else {
            tail.next = temp;
            tail = temp;
        }
        NodeCoordinate.counter++;
    }

    double getDistance(NodeCoordinate Node1, NodeCoordinate Node2) {
        double dlon = Node2.yCo - Node1.yCo;
        double dlat = Node2.xCo - Node1.xCo;
        double latDistance = Math.toRadians(dlat);
        double lonDistance = Math.toRadians(dlon);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(Node1.xCo)) * Math.cos(Math.toRadians(Node2.xCo)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = 6373 * c;
        return distance;
    }

    void convertToMatrix() {
        temp = start;
        NodeCoordinate tempX = start;
        for (int i = 0; i < NodeCoordinate.counter; i++) {
            for (int j = i; j < NodeCoordinate.counter; j++) {
                distMatrix[i][j] = distMatrix[j][i] = getDistance(temp, tempX);
                tempX = tempX.next;
            }
            temp = tempX = temp.next;
        }
        //Displaying
        for (int i = 0; i < NodeCoordinate.counter; i++) {
            System.out.println("");
            for (int j = 0; j < NodeCoordinate.counter; j++) {
                System.out.print("\t");
                System.out.print("   " + df.format(distMatrix[i][j]));
            }
        }
    }

    void solveTSP(int noOfNodes) {
        TSP tspIn = new TSP(noOfNodes);
        tspIn.setDistanceMatrix(distMatrix);
        tspIn.TSPPathSetter();
        System.out.println("\tMinimum Distance: " + tspIn.minimumDistance());
        System.out.print("\n");
    }
}
