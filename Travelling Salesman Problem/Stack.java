/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Anuj Shah
 */
public class Stack {

    protected int size = 0;
    public int i;
    Node tail, head, temp;

    void push(int value) //to push element into the stack
    {
        size++;
        Node node = new Node();
        node.data = value;
        if (size != 1) //when there are more than one element in the list
        {
            tail.next = node;  //next part of tail would be pointing towards the newly created object of stack
            tail = node;
        } else //when there is one element in the list
        {
            tail = head = node;  //tail and head both would be pointing towards object of stack
        }
        node.next = null;
    }

    int pop() //popping the element from the list and return type is boolean
    {
        int no; //temporary no
        if (size == 0) //when the stack is empty then it would return false
        {
            System.out.println("\tStack is empty.");
            return -1;
        } else //when there are more than one element in the list
        {
            no = tail.data; //no would be storing data
            if (size != 1) //when there are more than one element in the list
            {
                for (i = 1, temp = head; i < size - 1; i++) //at the end of the loop temp would be pointing towards second last element
                {
                    temp = temp.next;
                }
                tail = temp;  //tail would be pointing towards second last element
            } else //when there is only one element in the list then both head and tail would become null
            {
                tail = head = null;
            }
            size--; //size would be decremented by one
            return no;
        }
    }

    int peek() {
        if (size != 0) {
            return tail.data;
        }
        return -1;
    }

    void display() //displaying the elements of the stack
    {
        for (i = 0, temp = head; i < size; i++, temp = temp.next) //loop to the end of file
        {
            System.out.println("\tValue: " + temp.data);
        }
    }
}
