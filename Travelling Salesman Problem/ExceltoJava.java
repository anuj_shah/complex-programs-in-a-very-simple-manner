//importing apache poi libraries

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Anuj Shah
 */
public class ExceltoJava {  //excel to java class

    public Iterator<Cell> cellIterator; //this iterator moves through the cells
    DistMatrix dist = new DistMatrix(); //Used the concept of containership //created obejct of DistMatrix class

    Object getCellValue(Cell cell) {    //just returns value according to its type
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                return cell.getStringCellValue();

            case Cell.CELL_TYPE_BOOLEAN:
                return cell.getBooleanCellValue();

            case Cell.CELL_TYPE_NUMERIC:
                return cell.getNumericCellValue();
        }

        return null;
    }

    void readFile(String fileName, String cityName) throws IOException {    //reads file i.e excel file 
        FileInputStream inputStream = new FileInputStream(new File(fileName));

        Workbook workbook = new XSSFWorkbook(inputStream);
        Sheet firstSheet = workbook.getSheetAt(0);
        Iterator<Row> iterator = firstSheet.iterator();

        while (iterator.hasNext()) {    //this iterate through the rows
            Row nextRow = iterator.next();
            cellIterator = nextRow.cellIterator();

            while (cellIterator.hasNext()) {    //until the record is not matched this loop would run
                Cell nextCell = cellIterator.next();
                int columnIndex = nextCell.getColumnIndex();
                if (columnIndex == 1 && nextCell.getStringCellValue().equals(cityName)) {   //here the record is being matched
                    double xTemp, yTemp;
                    nextCell = cellIterator.next();
                    xTemp = (double) getCellValue(nextCell);
                    nextCell = cellIterator.next();
                    yTemp = (double) getCellValue(nextCell);
                    System.out.println("\tLatitude: " + xTemp);
                    System.out.println("\tLongitude: " + yTemp);
                    dist.createNode(xTemp, yTemp);  //if the record gets matched then their latitudes and longitudes are passed into createNode function
                    break;
                }
            }
        }
    }

    void solveTSP(int nodes) {  //solveTSP function
        dist.initDist();    //initDist is called which calculated distance from latitude and longitude
        System.out.print("\n\n");
        dist.solveTSP(nodes);   //solveTSP function DistMatrix class is called
    }
}
