/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Anuj Shah
 */
public class Heap { //Heap data structure class

    private double[] data;  //data of heap
    int size;
    public int leftC, rightC, min, j;
    double temp;

    Heap(int sizeOfHeap) {  //Heap is intialised with this constructor
        size = sizeOfHeap;
        data = new double[size + 1];
    }

    void copyData(double[] distance) {  //data of min distance is copied using this function
        System.arraycopy(distance, 1, data, 1, size);
    }

    void buildMinHeap(int size) {   //function to build minHeap
        for (j = size / 2; j >= 1; j--) {
            Heapify(j);
        }
    }

    void Heapify(int i) {   //heapify to build min priority queue
        leftC = 2 * i;
        rightC = leftC + 1;
        if (leftC <= size && data[leftC] < data[i]) {   //this condition sees that no left child is smaller than its parent
            min = leftC;
        } else {
            min = i;
        }
        if (rightC <= size && data[rightC] < data[min]) {   //this condition sees that no right child is smaller than its parent
            min = rightC;
        }
        if (min != i) {
            temp = data[min];
            data[min] = data[i];
            data[i] = temp;
            Heapify(min);
        }
    }

    double minElement() {   //to return minElemnt of the heap
        temp = data[1];
        data[1] = data[size];
        size--;
        double[] tempData = new double[size + 1];
        System.arraycopy(data, 1, tempData, 1, size);
        data = new double[size + 1];
        System.arraycopy(tempData, 1, data, 1, size);
        return temp;
    }
}
