
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Anuj Shah
 */
public class TSPSolver {    //main class

    public static void main(String[] args) throws Exception {   //main function
        int choice, nodes, i;
        String cityName, fileName;
        Scanner input = new Scanner(System.in);
        System.out.print("\tEnter no of cities: "); //input from user as no of cities
        nodes = input.nextInt();
        System.out.print("\n");
        //DistMatrix city = new DistMatrix(); 
        TSP tsp = new TSP(nodes);   //created object of class TSP
        ExceltoJava etj = new ExceltoJava();    //created object of class ExceltoJava
        for (i = 0; i < nodes; i++) {   //this loop takes cities as input
            System.out.print("\tEnter city name: ");
            cityName = input.next();
            etj.readFile("Data.xlsx", cityName);
            System.out.print("\n");
        }
        etj.solveTSP(nodes);    //solveTSP function of ExceltoJava class is called

        /*tsp.TSPPathSetter();
         System.out.println("\nMinimum Distance: " + tsp.minimumDistance());*/
    }
}
