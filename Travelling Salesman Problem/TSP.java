/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Anuj Shah
 */
public class TSP {  //the algorithm of O(n^3) is implemented in this class

    private final int noOfNodes;    //noOfNodes which is equivalent to no of cities
    double[][] distanceMatrix;  //distanceMatrix to store the distances between cities
    public int temp1, temp2, i, j, k;
    double minElement, min;
    boolean flag;
    int[] visited;  //visited to keep track of visited nodes
    double[] distance;
    Stack[] stack;

    TSP(int nodes) {    //TSP constructor to initialise various values
        noOfNodes = nodes;
        distanceMatrix = new double[noOfNodes + 1][noOfNodes + 1];
        visited = new int[noOfNodes + 1];
        stack = new Stack[noOfNodes + 1];
        distance = new double[noOfNodes + 1];
        for (j = 1; j <= noOfNodes; j++) {
            stack[j] = new Stack();
            distance[j] = 0;
        }
    }

    void setVisitZero() {   //setting all visited nodes to zero
        for (j = 1; j <= noOfNodes; j++) {
            visited[j] = 0;
        }
    }

    void setDistanceMatrix(double[][] distMatrix) { //setting distance matrix
        for (i = 1; i <= noOfNodes; i++) {
            for (j = i; j <= noOfNodes; j++) {
                distanceMatrix[i][j] = distanceMatrix[j][i] = distMatrix[i - 1][j - 1];
            }
        }
    }

    void TSPPathSetter() {  //to calculate shortest path
        for (i = 1; i <= noOfNodes; i++) {
            k = 0;
            visited[i] = 1;
            stack[i].push(i);   //first node is always pushed into the stack
            for (;;) {
                temp1 = stack[i].peek();
                min = 9999999;
                flag = false;
                for (j = 1; j <= noOfNodes; j++) {  //it finds shortest unvisited node from current node
                    if (min > distanceMatrix[temp1][j] && distanceMatrix[temp1][j] != 0 && visited[j] != 1) {
                        min = distanceMatrix[temp1][j];
                        temp2 = j;
                        flag = true;
                    }
                }
                if (flag) { //if any unvisited node is visited then the following data gets updated
                    stack[i].push(temp2);   //that node gets pushed into the stack
                    flag = false;   //flag is resetted
                    visited[temp2] = 1; //that node is marked as visited 
                }
                if (stack[i].size == noOfNodes) {   //when all the nodes are visited then the while loop gets break
                    break;
                }
            }
            stack[i].push(i);   //finally first node is again pushed into the stack so that the journey gets completed
            System.out.println("\tPath " + i + ": ");
            stack[i].display(); //Paths are displayed 
            setVisitZero();
            while (k < noOfNodes) { //to calculate distance
                distance[i] += distanceMatrix[stack[i].pop()][stack[i].peek()];
                k++;
            }
            System.out.println("\tDistance: " + distance[i]);   //printing distance
            System.out.print("\n");
        }
    }

    double minimumDistance() {  //to calculate minimum distance
        Heap heap = new Heap(noOfNodes);    //created object of heap class //concept of containership
        heap.copyData(distance);    //distance becomes data for heap
        heap.buildMinHeap(noOfNodes);   //min priority queue is generated from that data
        minElement = heap.minElement(); //getting min element which becomes shortest distance
        return minElement;  //returning that element
    }
}
