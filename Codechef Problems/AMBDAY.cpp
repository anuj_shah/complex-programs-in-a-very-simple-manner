#include<iostream>
#include<cstdlib>
#include<cmath>

using namespace std;

int minimum(int *array, int size){
	int min;
	min = array[0];
	for(int z=0; z<size; z++){
		if(array[z] < min)
			min = array[z];	
	}
	return min;
}

int main(){
	int T, F, H, W;
	int *P;
	int *A;
	cin>>T;
	for(int i=0; i<T; i++){
		P = new int[F];
		A = new int[F+1];
		cin>>F;
		cin>>H;
		cin>>W;
		for(int j=0; j<F; j++){
			cin>>P[j];
		}
		A[0] = P[0]*H;
		A[F] = (W-P[F-1])*H;
		if(F!=1){
			for(int k=0; k<F-1; k++){
				A[k+1] = abs((P[k+1]-P[k])*H);
			}
		}
		cout<<minimum(A, F+1)<<"\n";
	}
}