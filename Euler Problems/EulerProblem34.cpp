#include <iostream>

using namespace std;

int factorial(int number){
    int result;
    if(number == 0)
        return 1;
    else
        result = number * factorial(number - 1);
    return result;
}

bool curiousNo(int input){
    int sumX = 0, tempInput = input, mod;
    while(tempInput > 0){
        mod = tempInput%10;
        sumX += factorial(mod);
        tempInput /= 10;
    }
    if(sumX%input == 0)
        return true;
    return false;
}

int main()
{
    int N, sum=0;
    cin>>N;
    for(int i=10; i<N; i++){
        if(curiousNo(i))
            sum+=i;
    }
    cout<<sum<<endl;
    return 0;
}
