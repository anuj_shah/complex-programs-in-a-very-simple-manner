#include <iostream>

using namespace std;

long long int sum(long long int n){
    n--;
    long long int n1,n2,n3,sum;
	n1 = n/3;
	n2 = n/5;
	n3 = n/15;
	sum = (3* n1*(n1+1))/2 + (5*n2*(n2+1))/2 - (15*n3*(n3+1))/2;
	return sum;
}

int main()
{
    int T;
    long long int N;
    cin>>T;
    for(int i=0; i<T; i++){
        cin>>N;
        cout<<sum(N)<<endl;
    }
    return 0;
}
