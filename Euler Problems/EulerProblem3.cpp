#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

long long int factor(long long int n){
	long long int temp = 1;
	for (long long int i = 2; i <= sqrt(n); ++i)
	{
		while(n%i == 0){
			temp = i;
			n /= i;
		}
		if(n>1) temp = n;
	}
	return temp;
}

int main(int argc, char const *argv[])
{
	int t;
	cin>>t;
	while(t--){
		long long int n;
		cin>>n;
		cout<<factor(n)<<"\n";
	}
	return 0;
}
