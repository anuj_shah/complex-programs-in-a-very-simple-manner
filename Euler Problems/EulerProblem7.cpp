#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

bool isPrime(int n){
    for(int i=2; i*i<=n; i++){
        if(i!=n && n%i == 0)
            return false;
    }
    return true;
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int T, N, count=0;
    cin>>T;
    while(T--){
        cin>>N;
        for(int j=2; ; j++){
            if(isPrime(j))
                count++;
            if(count == N){
                cout<<j;
                break;
            }
        }
        count=0;
        cout<<endl;
    }
    return 0;
}
