#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int T, N, coin;
    //long long int* ways;
    int div = 1000000007;
    int value[] = {1, 2, 5, 10, 20, 50, 100, 200};
    int ways[100009];
    cin>>T;
    while(T--){
        cin>>N;
        //ways = new long long int[N+1];
        for(int z=0; z<=N; z++){
            ways[z] = 0;
        }
        ways[0] = 1;
        for(int i=0; i<8; i++){
            coin = value[i];
            for(int j=coin; j<=N; j++){
                ways[j] =  (ways[j] + ways[j-coin])%div;
            }
        }
        cout<<ways[N]<<endl;
    }
    return 0;
}
