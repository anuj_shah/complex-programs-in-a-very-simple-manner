#include<stdio.h>

int main()

{
	int T, N, *list, *countOdd, *countEven, *countTotal, i, j;	//variables
	printf("Input\n");
	do
	{
		scanf("%d", &T);
	}while(T>20);
	//dynamically allocating size
	countOdd = (int*)malloc(T*sizeof(int));
	countEven = (int*)malloc(T*sizeof(int));
	countTotal = (int*)malloc(T*sizeof(int));
	for(i=0; i<T; i++)	//taking input according to test cases
	{
		do
		{
			scanf("%d", &N);	//taking no of inputs less than 10^5
		}while(N>100000);
		list = (int*)malloc(N*sizeof(int));
		for(j=0, countEven[i]=0, countOdd[i]=0; j<N; j++)
		{
			do
			{
				scanf("%d", &list[j]);	//taking binary input			
			}while(list[j]>1);
			if(list[j] == 0)
				countEven[i]++;
			else
				countOdd[i]++;
		}
		//checking different conditions
		if(countEven[i]%2 == 0 && countOdd[i]%2 == 0)
			countTotal[i] = countEven[i];
		else if(countEven[i]%2 == 0 && countOdd[i]%2 != 0)
			countTotal[i] = countOdd[i];
		else if(countEven[i]%2 != 0 && countOdd[i]%2 == 0)
			countTotal[i] = countEven[i];
		else
			countTotal[i] = countOdd[i];
	}
	printf("Output\n");
	for(i=0; i<T; i++)
		printf("%d\n", countTotal[i]);	//printing the results
	return 0;
}